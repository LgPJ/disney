package jedi.disney.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity
@Table (name = "personaje")
public class Personaje implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @NonNull
    private Integer idPersonaje;

    private ArrayList<Byte> imagen;
    
    @NonNull
    private String nombre;

    @NonNull
    private Integer edad;

    @NonNull
    private float peso;

    private String historia;


    @JoinTable(name = "personaje_pelicula", 
    joinColumns = @JoinColumn(name = "id_personaje", nullable = false),
    inverseJoinColumns = @JoinColumn(name = "id_pelicula", nullable = false))
    @ManyToMany
    private List<Pelicula> pelicula;
    

    public Personaje() {
    }

    public Personaje(Integer idPersonaje, ArrayList<Byte> imagen, String nombre, Integer edad, float peso, String historia, List<Pelicula> pelicula) {
        this.idPersonaje = idPersonaje;
        this.imagen = imagen;
        this.nombre = nombre;
        this.edad = edad;
        this.peso = peso;
        this.historia = historia;
        this.pelicula = pelicula;
    }

    public Integer getIdPersonaje() {
        return this.idPersonaje;
    }

    public void setIdPersonaje(Integer idPersonaje) {
        this.idPersonaje = idPersonaje;
    }

    public ArrayList<Byte> getImagen() {
        return this.imagen;
    }

    public void setImagen(ArrayList<Byte> imagen) {
        this.imagen = imagen;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getEdad() {
        return this.edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    public float getPeso() {
        return this.peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public String getHistoria() {
        return this.historia;
    }

    public void setHistoria(String historia) {
        this.historia = historia;
    }

    public List<Pelicula> getPelicula() {
        return this.pelicula;
    }

    public void setPelicula(List<Pelicula> pelicula) {
        this.pelicula = pelicula;
    }

    public Personaje idPersonaje(Integer idPersonaje) {
        setIdPersonaje(idPersonaje);
        return this;
    }

    public Personaje imagen(ArrayList<Byte> imagen) {
        setImagen(imagen);
        return this;
    }

    public Personaje nombre(String nombre) {
        setNombre(nombre);
        return this;
    }

    public Personaje edad(Integer edad) {
        setEdad(edad);
        return this;
    }

    public Personaje peso(float peso) {
        setPeso(peso);
        return this;
    }

    public Personaje historia(String historia) {
        setHistoria(historia);
        return this;
    }

    public Personaje pelicula(List<Pelicula> pelicula) {
        setPelicula(pelicula);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Personaje)) {
            return false;
        }
        Personaje personaje = (Personaje) o;
        return Objects.equals(idPersonaje, personaje.idPersonaje) && Objects.equals(imagen, personaje.imagen) && Objects.equals(nombre, personaje.nombre) && Objects.equals(edad, personaje.edad) && peso == personaje.peso && Objects.equals(historia, personaje.historia) && Objects.equals(pelicula, personaje.pelicula);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPersonaje, imagen, nombre, edad, peso, historia, pelicula);
    }

    @Override
    public String toString() {
        return "{" +
            " idPersonaje='" + getIdPersonaje() + "'" +
            ", imagen='" + getImagen() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", edad='" + getEdad() + "'" +
            ", peso='" + getPeso() + "'" +
            ", historia='" + getHistoria() + "'" +
            ", pelicula='" + getPelicula() + "'" +
            "}";
    }
    
    
}
