package jedi.disney.Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity
@Table(name = "genero")
public class Genero implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idGenero;

    @NonNull
    private String nombre;

    private ArrayList<Byte> imagen;


    public Genero() {
    }
    
    public Genero(int idGenero, String nombre, ArrayList<Byte> imagen) {
        this.idGenero = idGenero;
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public int getIdGenero() {
        return this.idGenero;
    }

    public void setIdGenero(int idGenero) {
        this.idGenero = idGenero;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ArrayList<Byte> getImagen() {
        return this.imagen;
    }

    public void setImagen(ArrayList<Byte> imagen) {
        this.imagen = imagen;
    }

    public Genero idGenero(int idGenero) {
        setIdGenero(idGenero);
        return this;
    }

    public Genero nombre(String nombre) {
        setNombre(nombre);
        return this;
    }

    public Genero imagen(ArrayList<Byte> imagen) {
        setImagen(imagen);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Genero)) {
            return false;
        }
        Genero genero = (Genero) o;
        return idGenero == genero.idGenero && Objects.equals(nombre, genero.nombre) && Objects.equals(imagen, genero.imagen);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idGenero, nombre, imagen);
    }

    @Override
    public String toString() {
        return "{" +
            " idGenero='" + getIdGenero() + "'" +
            ", nombre='" + getNombre() + "'" +
            ", imagen='" + getImagen() + "'" +
            "}";
    }
    
    
}
