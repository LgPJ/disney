package jedi.disney.Model;

import java.io.Serializable;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.springframework.lang.NonNull;

@Entity
@Table(name = "pelicula")
public class Pelicula implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
    @NonNull
    private Integer idPelicula;

    private ArrayList<Byte> imagen;

    @NonNull
    private String titulo;
    
    private Date fechaCreacion;

    @NonNull
    private int calificacion;

    @ManyToMany(mappedBy = "pelicula")
    private List<Personaje> personaje;


    public Pelicula() {
    }

    public Pelicula(int idPelicula, ArrayList<Byte> imagen, String titulo, Date fechaCreacion, int calificacion, List<Personaje> personaje) {
        this.idPelicula = idPelicula;
        this.imagen = imagen;
        this.titulo = titulo;
        this.fechaCreacion = fechaCreacion;
        this.calificacion = calificacion;
        this.personaje = personaje;
    }

    public int getIdPelicula() {
        return this.idPelicula;
    }

    public void setIdPelicula(int idPelicula) {
        this.idPelicula = idPelicula;
    }

    public ArrayList<Byte> getImagen() {
        return this.imagen;
    }

    public void setImagen(ArrayList<Byte> imagen) {
        this.imagen = imagen;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public Date getFechaCreacion() {
        return this.fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public int getCalificacion() {
        return this.calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    public List<Personaje> getPersonaje() {
        return this.personaje;
    }

    public void setPersonaje(List<Personaje> personaje) {
        this.personaje = personaje;
    }

    public Pelicula idPelicula(int idPelicula) {
        setIdPelicula(idPelicula);
        return this;
    }

    public Pelicula imagen(ArrayList<Byte> imagen) {
        setImagen(imagen);
        return this;
    }

    public Pelicula titulo(String titulo) {
        setTitulo(titulo);
        return this;
    }

    public Pelicula fechaCreacion(Date fechaCreacion) {
        setFechaCreacion(fechaCreacion);
        return this;
    }

    public Pelicula calificacion(int calificacion) {
        setCalificacion(calificacion);
        return this;
    }

    public Pelicula personaje(List<Personaje> personaje) {
        setPersonaje(personaje);
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Pelicula)) {
            return false;
        }
        Pelicula pelicula = (Pelicula) o;
        return idPelicula == pelicula.idPelicula && Objects.equals(imagen, pelicula.imagen) && Objects.equals(titulo, pelicula.titulo) && Objects.equals(fechaCreacion, pelicula.fechaCreacion) && calificacion == pelicula.calificacion && Objects.equals(personaje, pelicula.personaje);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPelicula, imagen, titulo, fechaCreacion, calificacion, personaje);
    }

    @Override
    public String toString() {
        return "{" +
            " idPelicula='" + getIdPelicula() + "'" +
            ", imagen='" + getImagen() + "'" +
            ", titulo='" + getTitulo() + "'" +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            ", calificacion='" + getCalificacion() + "'" +
            ", personaje='" + getPersonaje() + "'" +
            "}";
    }

}
