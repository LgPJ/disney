package jedi.disney.Service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jedi.disney.Model.Pelicula;
import jedi.disney.Repository.PeliculaRepo;

@Service
public class PeliculaServi {
    
    @Autowired
    PeliculaRepo peliculaRepo;

    public ArrayList<Pelicula> findMovie(){
        return (ArrayList<Pelicula>) peliculaRepo.findAll();
    }

    public Pelicula saveMovie(Pelicula pelicula){
        return peliculaRepo.save(pelicula);

    }

    public Pelicula updateMovie(Pelicula pelicula){
        return peliculaRepo.save(pelicula);
    }

    public boolean deleteMovie(Integer idPelicula){
        try{
            peliculaRepo.deleteById(idPelicula);
            return true;
        } catch (Exception e){
            return false;
        }
    }
    
   public ArrayList<Pelicula> findTitulo(String titulo){
    	return peliculaRepo.findByTitulo(titulo);
    }
    
    /*public List<Pelicula> filterFecha(Date fecha){
    	return peliculaRepo.filterFecha(fecha);
    }*/
}
