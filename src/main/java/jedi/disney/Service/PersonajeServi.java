package jedi.disney.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jedi.disney.Model.Personaje;
import jedi.disney.Repository.PersonajeRepo;

@Service
public class PersonajeServi{

    @Autowired
    PersonajeRepo personajeRepo;
    
    public ArrayList<Personaje> findByCharacter(){
    	return (ArrayList<Personaje>) personajeRepo.findAll();
    }

    public Personaje saveCharacter(Personaje personaje) {
    	return personajeRepo.save(personaje);
    }
    
    public Personaje updateCharacter(Personaje personaje) {
    	return personajeRepo.save(personaje);
    }
    
    public boolean deleteCharacter(Integer idPersonaje) {
    	try{
            personajeRepo.deleteById(idPersonaje);
            return true;
        } catch (Exception e){
            return false;
        }
    }
    
    
    public Optional<Personaje> obtenerPersona(Integer idPersonaje){
        return personajeRepo.findById(idPersonaje);
    }

    public ArrayList<Personaje> findNombre(String nombre){
        return personajeRepo.findByNombre(nombre);
    }

    public ArrayList<Personaje> findByEdad(Integer edad){
        
        ArrayList<Personaje> personaje = personajeRepo.findByEdad(edad);
        return personaje;
        
    }

    public List<Personaje> findByPeso(float peso){
        return personajeRepo.findByPeso(peso);
    }
}
