package jedi.disney.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import jedi.disney.Model.Usuario;
import jedi.disney.Repository.UsuarioRepo;

@Service
public class UsuarioServi{

    @Autowired
    BCryptPasswordEncoder passwordEncoder;

    @Autowired
    UsuarioRepo usuarioRepo;

    public Usuario saveUsername(Usuario usuario){
        usuario.setPassword(passwordEncoder.encode(usuario.getPassword()));
        return usuarioRepo.save(usuario);
    }

    public List<Usuario> obtenerUsuario(String username){
        return usuarioRepo.findByUsername(username);
    } 
    
}