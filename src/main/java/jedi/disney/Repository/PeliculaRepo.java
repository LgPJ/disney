package jedi.disney.Repository;

import java.util.ArrayList;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.disney.Model.Pelicula;

@Repository
public interface PeliculaRepo extends CrudRepository<Pelicula, Integer>{
    
	ArrayList<Pelicula> findByTitulo(String titulo);
	
	//ArrayList<Pelicula> filterFecha(Date fecha);
}
