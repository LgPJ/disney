package jedi.disney.Repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.disney.Model.Personaje;

@Repository
public interface PersonajeRepo extends CrudRepository<Personaje, Integer>{
    
    ArrayList<Personaje> findByNombre(String nombre);

    ArrayList<Personaje> findByEdad(Integer edad);

    public List<Personaje> findByPeso(float peso);
}
