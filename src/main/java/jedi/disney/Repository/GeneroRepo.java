package jedi.disney.Repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.disney.Model.Genero;

@Repository
public interface GeneroRepo extends CrudRepository<Genero, Integer>{
    
}
