package jedi.disney.Repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import jedi.disney.Model.Usuario;

@Repository
public interface UsuarioRepo extends CrudRepository<Usuario, Integer>{

   List<Usuario> findByUsername(String username);
    
}

