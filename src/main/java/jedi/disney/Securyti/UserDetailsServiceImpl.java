package jedi.disney.Securyti;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.User.UserBuilder;
import org.springframework.stereotype.Service;

import jedi.disney.Model.Usuario;
import jedi.disney.Repository.UsuarioRepo;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    
    @Autowired
    UsuarioRepo usuarioRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        List<Usuario> usuario = usuarioRepo.findByUsername(username);
        UserBuilder builder = null;

        if(usuario != null){
            builder = User.withUsername(username);
            builder.disabled(false);
            builder.password(((User) usuario).getPassword());
            builder.authorities(new SimpleGrantedAuthority("ROLE_USER"));
            
        }
        else{
            throw new UsernameNotFoundException("Usuario no encontrado");
        }

        return builder.build();

    }
}
