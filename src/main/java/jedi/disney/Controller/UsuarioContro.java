package jedi.disney.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import jedi.disney.Model.Usuario;
import jedi.disney.Service.UsuarioServi;

@RestController
@RequestMapping("/auth")
public class UsuarioContro {

    @Autowired
    UsuarioServi usuarioService;

    @PostMapping("/register")
    public Usuario saveUser (@RequestBody Usuario usuario){
        return usuarioService.saveUsername(usuario);
    }

    @GetMapping("/login/{username}")
    public List<Usuario> findUser(@PathVariable ("username") String username){
        return usuarioService.obtenerUsuario(username);
    }
}

