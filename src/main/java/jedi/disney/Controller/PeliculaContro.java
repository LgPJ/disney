package jedi.disney.Controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jedi.disney.Model.Pelicula;
import jedi.disney.Service.PeliculaServi;

@RestController
@RequestMapping("/movie")
public class PeliculaContro {

    @Autowired
    PeliculaServi peliculaServi;

    @GetMapping("/obtener")//Error de impresion
    public ArrayList<Pelicula> findByMovie(){
        return peliculaServi.findMovie();
    }

    @PostMapping("/guardar")//Funciona
    public Pelicula save(@RequestBody Pelicula pelicula){
        return peliculaServi.saveMovie(pelicula);
    }

    @PutMapping("/actualizar")//Funciona
    public Pelicula update(@RequestBody Pelicula pelicula){
        return peliculaServi.updateMovie(pelicula);
    }

    @DeleteMapping("/eliminar/{idPelicula}")
        public String delete(@PathVariable("idPelicula") Integer idPelicula){
            boolean ok = this.peliculaServi.deleteMovie(idPelicula);
        if(ok){
            return "Pelicula eliminada";
        } else {
            return "Pelicula no eliminada";
        }
    }
    
    @GetMapping()
    public ArrayList<Pelicula> title(@RequestParam ("titulo") String titulo){
    	return peliculaServi.findTitulo(titulo);
    }
    
    /*@GetMapping()
    public List<Pelicula> filter(@RequestParam("fecha") Date fecha){
    	return peliculaServi.filterRank(fecha);
    }*/
    
}
