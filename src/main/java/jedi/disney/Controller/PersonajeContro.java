package jedi.disney.Controller;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jedi.disney.Model.Personaje;
import jedi.disney.Service.PersonajeServi;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;


@RestController
@RequestMapping("/characters")
public class PersonajeContro {
    
    @Autowired
    PersonajeServi personajeServi;


    @GetMapping("/obtener")
    public ArrayList<Personaje> findCharacter(){
    	return personajeServi.findByCharacter();
    }
    
    @PostMapping("/guardar")
    public Personaje save(@RequestBody Personaje personaje) {
    	return personajeServi.saveCharacter(personaje);
    }
    
    @PutMapping("/actualizar")
    public Personaje update(@RequestBody Personaje personaje){
    	return personajeServi.updateCharacter(personaje);
    }
    @DeleteMapping("/elimina/{idPersonaje}")
    public String delete(@PathVariable("idPersonaje") Integer IdPersonaje) {
    	boolean ok = personajeServi.deleteCharacter(IdPersonaje);
    	
    	if(ok) {
    		return "Personaje Eliminado";
    	}else {
    		return "Personaje no Eliminado";
    	}
    }
     
    @GetMapping("/obtener/{idPersonaje}")
    public Optional<Personaje> findCharacters(@PathVariable("idPersonaje") Integer idPersonaje){
        return this.personajeServi.obtenerPersona(idPersonaje);
    }

    @GetMapping("{nombre}")
    public ArrayList<Personaje> buscarNombre(@RequestParam("nombre") String nombre){
        return this.personajeServi.findNombre(nombre);
    }

    
    @GetMapping("{edad}")
    public ArrayList<Personaje> buscarEdad(@RequestParam("edad") Integer edad){
        return this.personajeServi.findByEdad(edad);
    }

    @GetMapping("{peso}")
    public List<Personaje> buscarPeso(@RequestParam("peso") float peso){
        return this.personajeServi.findByPeso(peso);
    }
    
}
